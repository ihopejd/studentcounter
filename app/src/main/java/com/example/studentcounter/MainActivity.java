package com.example.studentcounter;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.util.Log;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    private Integer counter = 0;
    TextView counterView;
    public static final String TAG = "StartActivity";

    /** Вызывается при создании Активности
     *
     * @param savedInstanceState объект, который сохраняет в себе состояние Активити
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        counterView = findViewById(R.id.txt_counter);
        Log.d(TAG, "onCreate");
    }

    /** Вызывается, когда Активность стала видимой
     *
     */
    @Override
    public void onStart() {
        super.onStart();
        resetUI();
        Toast.makeText(this, "onStart()", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onStart");
    }

    /** Вызывается при восстанавлении Активности из неактивного состояния
     *  Восстанавливает приостановленные обновления UI
     */
    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "onResume()", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onResume");
    }

    /** Вызывается перед выходом из активного состояния
     *
     */
    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "onPause()", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onPause");
    }

    /** Вызывается перед выходом из видимого состояния
     *
     */
    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "onStop()", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onStop");
    }

    /** Вызывается перед уничтожением Активити
     *
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "onDestroy()", Toast.LENGTH_LONG).show();
        Log.d(TAG, "onDestroy");
    }

    /** Вызывается перед выходом из активного состояния, позволяя сохранить состояние в объекте savedInstanceState
     *
     * @param savedInstanceState объект, который сохраняет в себе состояние Активити
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("counter", counter);
        Log.d(TAG, "onSaveInstanceState");
    }

    /** Вызывается после завершения метода onCreate
     *
     * @param savedInstanceState объект, который сохраняет в себе состояние Активити
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("counter")) {
            counter = savedInstanceState.getInt("counter", counter);
            counterView.setText(String.valueOf(counter));
        }
        Log.d(TAG, "onRestoreInstanceState");
    }

    public void onClickBtnAddStudents(View view) {
        counter++;
        counterView.setText(String.valueOf(counter));
    }

    private void resetUI() {
        ((TextView) findViewById(R.id.txt_counter)).setText(counter.toString());
        Log.d(TAG, "resetUI");
    }
}
